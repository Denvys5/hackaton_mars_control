package com.denvys5.hackaton_mars_control_v2.model;

public class JsonMessageDTO {
    private float linearX;
    private float linearY;
    private float linearZ;
    private float accX;
    private float accY;
    private float accZ;
    private float axisX;
    private float axisY;
    private float axisZ;
    private int angle;
    private int strength;

    private boolean Drone_loop_backwards;
    private boolean Drone_loop_forward;
    private boolean barrel;

    public JsonMessageDTO(SensorsData sensorsData, CommandData commandData) {
        this.linearX = sensorsData.getLinearX();
        this.linearY = sensorsData.getLinearY();
        this.linearZ = sensorsData.getLinearZ();
        this.accX = sensorsData.getAccX();
        this.accY = sensorsData.getAccY();
        this.accZ = sensorsData.getAccZ();
        this.axisX = sensorsData.getAxisX();
        this.axisY = sensorsData.getAxisY();
        this.axisZ = sensorsData.getAxisZ();
        this.angle = sensorsData.getAngle();
        this.strength = sensorsData.getStrength();
        Drone_loop_backwards = commandData.isDrone_loop_backwards();
        Drone_loop_forward = commandData.isDrone_loop_forward();
        this.barrel = commandData.isBarrel();
    }

    public JsonMessageDTO(SensorsData sensorsData) {
        this.linearX = sensorsData.getLinearX();
        this.linearY = sensorsData.getLinearY();
        this.linearZ = sensorsData.getLinearZ();
        this.accX = sensorsData.getAccX();
        this.accY = sensorsData.getAccY();
        this.accZ = sensorsData.getAccZ();
        this.axisX = sensorsData.getAxisX();
        this.axisY = sensorsData.getAxisY();
        this.axisZ = sensorsData.getAxisZ();
        this.angle = sensorsData.getAngle();
        this.strength = sensorsData.getStrength();
    }
}
