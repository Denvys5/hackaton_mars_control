package com.denvys5.hackaton_mars_control_v2.model;

public class CommandData {
    private boolean Drone_loop_backwards;
    private boolean Drone_loop_forward;
    private boolean barrel;

    public CommandData(boolean drone_loop_backwards, boolean drone_loop_forward, boolean barrel) {
        Drone_loop_backwards = drone_loop_backwards;
        Drone_loop_forward = drone_loop_forward;
        this.barrel = barrel;
    }

    public boolean isDrone_loop_backwards() {
        return Drone_loop_backwards;
    }

    public boolean isDrone_loop_forward() {
        return Drone_loop_forward;
    }

    public boolean isBarrel() {
        return barrel;
    }
}
