package com.denvys5.hackaton_mars_control_v2.model;

public class SensorsData {
    private float linearX;
    private float linearY;
    private float linearZ;
    private float accX;
    private float accY;
    private float accZ;
    private float axisX;
    private float axisY;
    private float axisZ;
    private int angle;
    private int strength;

    public SensorsData(float linearX, float linearY, float linearZ, float accX, float accY, float accZ, float axisX, float axisY, float axisZ, int angle, int strength) {
        this.linearX = linearX;
        this.linearY = linearY;
        this.linearZ = linearZ;
        this.accX = accX;
        this.accY = accY;
        this.accZ = accZ;
        this.axisX = axisX;
        this.axisY = axisY;
        this.axisZ = axisZ;
        this.angle = angle;
        this.strength = strength;
    }

    public float getLinearZ() {
        return linearZ;
    }

    public float getAccZ() {
        return accZ;
    }

    public float getLinearX() {
        return linearX;
    }

    public float getLinearY() {
        return linearY;
    }

    public float getAccX() {
        return accX;
    }

    public float getAccY() {
        return accY;
    }

    public float getAxisX() {
        return axisX;
    }

    public float getAxisY() {
        return axisY;
    }

    public float getAxisZ() {
        return axisZ;
    }

    public int getAngle() {
        return angle;
    }

    public int getStrength() {
        return strength;
    }
}
