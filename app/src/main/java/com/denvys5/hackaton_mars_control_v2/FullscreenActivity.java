package com.denvys5.hackaton_mars_control_v2;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.os.Handler;
import android.speech.RecognizerIntent;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.denvys5.hackaton_mars_control_v2.controller.MessageDataHolder;
import com.denvys5.hackaton_mars_control_v2.controller.SensorsDataSink;
import com.denvys5.hackaton_mars_control_v2.controller.SocketScheduledTask;
import com.denvys5.hackaton_mars_control_v2.controller.sensor.AccelerometerListener;
import com.denvys5.hackaton_mars_control_v2.controller.sensor.LinearAccelerometerListener;
import com.denvys5.hackaton_mars_control_v2.controller.SocketController;
import com.denvys5.hackaton_mars_control_v2.controller.sensor.MagneticFieldListener;
import com.denvys5.hackaton_mars_control_v2.model.CommandData;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Locale;

import io.github.controlwear.virtual.joystick.android.JoystickView;

/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */
public class FullscreenActivity extends AppCompatActivity{
    private SocketController socketController;
    private SocketScheduledTask scheduledTask;

    private Button buttonDebug1;
    private ImageButton buttonCalibrate;
    private ImageButton buttonVoice;
    private EditText serverip;
    private EditText scheduleTime;

    private SensorManager sensorManager;
    private Sensor sensorLinearAccelerometer;
    private Sensor sensorAccelerometer;
    private Sensor magneticField;
    private final Handler mHideHandler = new Handler();
    private static final int UI_ANIMATION_DELAY = 300;
    private boolean connected;
    private JoystickView joystickView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_fullscreen);

        socketController = SocketController.getInstance();

        Toast.makeText(getApplicationContext(),"Connecting to server", Toast.LENGTH_LONG).show();

        sensorsSetup();

        viewFieldsSetup();

        listenerSetup();

        scheduledTask = new SocketScheduledTask();
        scheduledTask.start();

        hideUI();

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
    }

    private void listenerSetup() {
        serverip.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
            @Override
            public void afterTextChanged(Editable s) {
                Config.serverAddress = s.toString();
            }
        });

        scheduleTime.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
            @Override
            public void afterTextChanged(Editable s) {
                try{
                    Config.scheduleTime = Integer.parseInt(s.toString());
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        });

        buttonCalibrate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                System.out.println(new Gson().toJson(MessageDataHolder.getInstance().getMessage()));
                //Calibrate
            }
        });

        buttonVoice.setOnClickListener(new VoiceRecognition(this));

        buttonDebug1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(connected)
                    socketController.disconnect();
                else
                    socketController.setUp();
                connected=!connected;
            }
        });

        joystickView.setAutoReCenterButton(false);
        joystickView.setOnMoveListener(new JoystickView.OnMoveListener() {
            @Override
            public void onMove(int angle, int strength) {
                SensorsDataSink.getInstance().setJoystickData(angle, strength);
            }
        });
    }

    private void viewFieldsSetup() {
        buttonDebug1 = findViewById(R.id.button);
        buttonCalibrate = findViewById(R.id.calibrateButton);
        buttonVoice = findViewById(R.id.voiceCommandButton);

        serverip = findViewById(R.id.serverip);
        scheduleTime = findViewById(R.id.scheduleTime);

        joystickView = findViewById(R.id.joystick);
    }

    private void sensorsSetup() {
        sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        sensorLinearAccelerometer = sensorManager.getDefaultSensor(Sensor.TYPE_LINEAR_ACCELERATION);
        sensorManager.registerListener(new LinearAccelerometerListener(), sensorLinearAccelerometer, SensorManager.SENSOR_DELAY_NORMAL);

        sensorAccelerometer = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        sensorManager.registerListener(new AccelerometerListener(), sensorAccelerometer, SensorManager.SENSOR_DELAY_NORMAL);

        magneticField = sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
        sensorManager.registerListener(new MagneticFieldListener(), magneticField, SensorManager.SENSOR_DELAY_NORMAL);
    }

    public class VoiceRecognition implements View.OnClickListener {
        private Activity activity;

        public VoiceRecognition(Activity activity) {
            this.activity = activity;
        }

        public void startVoiceRecognitionActivity() {
            Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
            intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,
                    RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
            intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.ENGLISH);
            intent.putExtra(RecognizerIntent.EXTRA_PROMPT,
                    "Speech recognition demo");
            activity.startActivityForResult(intent, Config.VOICE_RECOGNITION_REQUEST_CODE);
        }

        public void onClick(View v) {
            startVoiceRecognitionActivity();
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == Config.VOICE_RECOGNITION_REQUEST_CODE && resultCode == RESULT_OK) {
            // Fill the list view with the strings the recognizer thought it
            // could have heard
            ArrayList matches = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
            // matches is the result of voice input. It is a list of what the
            // user possibly said.
            // Using an if statement for the keyword you want to use allows the
            // use of any activity if keywords match
            // it is possible to set up multiple keywords to use the same
            // activity so more than one word will allow the user
            // to use the activity (makes it so the user doesn't have to
            // memorize words from a list)
            // to use an activity from the voice input information simply use
            // the following format;
            // if (matches.contains("keyword here") { startActivity(new
            // Intent("name.of.manifest.ACTIVITY")

            MessageDataHolder messageDataHolder = MessageDataHolder.getInstance();
            if (matches != null) {
                for(Object result:matches){
                    if(((String) result).equalsIgnoreCase("Barrel")){
                        messageDataHolder.setCommandData(new CommandData(false, false, true));
                        System.out.println("We got a barrel");
                        break;
                    }else if(((String) result).equalsIgnoreCase("Drone loop forward")){
                        messageDataHolder.setCommandData(new CommandData(false, true, false));
                        System.out.println("We got a forward");
                        break;
                    }else if(((String) result).equalsIgnoreCase("Drone loop backwards")){
                        messageDataHolder.setCommandData(new CommandData(true, false, false));
                        System.out.println("We got a backward");
                        break;
                    }
                }
            }
            System.out.println(matches);
        }
    }

    private void hideUI() {
        // Hide UI first
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.hide();
        }

        // Schedule a runnable to remove the status and navigation bar after a delay
        mHideHandler.postDelayed(mHidePart2Runnable, UI_ANIMATION_DELAY);
    }

    private final Runnable mHidePart2Runnable = new Runnable() {
        @SuppressLint("InlinedApi")
        @Override
        public void run() {
            // Delayed removal of status and navigation bar

            // Note that some of these constants are new as of API 16 (Jelly Bean)
            // and API 19 (KitKat). It is safe to use them, as they are inlined
            // at compile-time and do nothing on earlier devices.

            View decorView = getWindow().getDecorView();
            decorView.setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LOW_PROFILE
                    | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
            );
        }
    };


}
