package com.denvys5.hackaton_mars_control_v2.controller;

import android.hardware.SensorManager;

import com.denvys5.hackaton_mars_control_v2.model.SensorsData;

public class SensorsDataSink {
    private static SensorsDataSink instance = new SensorsDataSink();
    public static SensorsDataSink getInstance(){
        return instance;
    }

    private SensorsDataSink() {
    }

    private float linearX;
    private float linearY;
    private float linearZ;
    private float accX;
    private float accY;
    private float accZ;
    private float axisX;
    private float axisY;
    private float axisZ;
    private float magX;
    private float magY;
    private float magZ;
    private int angle;
    private int strength;
    private float calibratedX;
    private float calibratedY;
    private float calibratedZ;

    public void setAcceleromenterData(float[] array){
        this.accX = array[0];
        this.accY = array[1];
        this.accZ = array[2];
    }

    public void setLinearAcceleromenterData(float[] array){
        this.linearX = array[0];
        this.linearY = array[1];
        this.linearZ = array[2];
    }

    public void setMagnetometerData(float[] array){
        this.magX = array[0];
        this.magY = array[1];
        this.magZ = array[2];
    }

    public void setJoystickData(int angle, int strength){
        this.angle = angle;
        this.strength = strength;
    }

    public SensorsData getCurrentData(){
        return new SensorsData(linearX, linearY, linearZ, accX, accY, accZ, axisX, axisY, axisZ, angle, strength);
    }

    public float[] generatePosition(){
        // Rotation matrix based on current readings from accelerometer and magnetometer.
        final float[] rotationMatrix = new float[9];
        SensorManager.getRotationMatrix(rotationMatrix, null,
                new float[]{accX, accY, accZ}, new float[]{magX, magY, magZ});

        // Express the updated rotation matrix as three orientation angles.
        final float[] orientationAngles = new float[3];
        SensorManager.getOrientation(rotationMatrix, orientationAngles);

        axisX = (float) (orientationAngles[0]*Math.PI/180D);
        axisY = (float) (orientationAngles[1]*Math.PI/180D);
        axisZ = (float) (orientationAngles[2]*Math.PI/180D);

        return orientationAngles;
    }
}
