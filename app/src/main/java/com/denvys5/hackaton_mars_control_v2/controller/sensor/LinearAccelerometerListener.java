package com.denvys5.hackaton_mars_control_v2.controller.sensor;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;

import com.denvys5.hackaton_mars_control_v2.controller.SensorsDataSink;


public class LinearAccelerometerListener implements SensorEventListener {
    private SensorsDataSink sensorsDataSink;

    public LinearAccelerometerListener() {
        sensorsDataSink = SensorsDataSink.getInstance();
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        sensorsDataSink.setLinearAcceleromenterData(event.values);
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }
}
