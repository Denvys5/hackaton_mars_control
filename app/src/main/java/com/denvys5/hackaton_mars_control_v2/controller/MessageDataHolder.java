package com.denvys5.hackaton_mars_control_v2.controller;

import com.denvys5.hackaton_mars_control_v2.model.CommandData;
import com.denvys5.hackaton_mars_control_v2.model.JsonMessageDTO;
import com.denvys5.hackaton_mars_control_v2.model.SensorsData;

import java.util.Objects;

public class MessageDataHolder {
    private static MessageDataHolder instance = new MessageDataHolder();
    public static MessageDataHolder getInstance(){
        return instance;
    }

    private SensorsDataSink sensorsDataSink;
    private CommandData commandData;

    private MessageDataHolder() {
        this.sensorsDataSink = SensorsDataSink.getInstance();
    }

    public JsonMessageDTO getMessage(){
        sensorsDataSink.generatePosition();
        SensorsData sensorsData = sensorsDataSink.getCurrentData();


        JsonMessageDTO jsonMessageDTO;
        if(Objects.nonNull(commandData)){
            jsonMessageDTO = new JsonMessageDTO(sensorsData, commandData);
        }else{
            jsonMessageDTO = new JsonMessageDTO(sensorsData);
        }
        commandData = null;
        return jsonMessageDTO;
    }

    public void setCommandData(CommandData commandData) {
        this.commandData = commandData;
    }
}
