package com.denvys5.hackaton_mars_control_v2.controller;

import com.denvys5.hackaton_mars_control_v2.controller.async.SocketConnectAsync;
import com.denvys5.hackaton_mars_control_v2.controller.async.SocketInputAsync;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.concurrent.ExecutionException;

public class SocketController {
    private static SocketController instance = new SocketController();
    public static SocketController getInstance(){
        return instance;
    }

    private SocketController() {
    }

    private Socket socket;
    private BufferedReader in;
    private PrintWriter out;
    SocketInputAsync socketInputAsync;

    public void setUp(){
        SocketConnectAsync socketConnectAsync = new SocketConnectAsync();
        socketConnectAsync.execute();
        try {
            socket = socketConnectAsync.get();
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }


        try {
            in = new BufferedReader(
                    new InputStreamReader(socket.getInputStream()));
            out = new PrintWriter(socket.getOutputStream(), true);

//            println("Name");//Send first msg to chat server
            System.out.println("Connected");
        } catch (IOException e) {
            e.printStackTrace();
        }


        socketInputAsync = new SocketInputAsync();
        socketInputAsync.execute(in);
    }

    public void println(final String string){
        new Thread(new Runnable() {
            @Override
            public void run() {
                try{
                    if(!socket.isClosed())
                        out.println(string);
                }catch (Exception e){
                }
            }
        }).start();
    }

    public void disconnect(){
        try {
            socket.close();
            in.close();
            socketInputAsync.cancel(true);
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
