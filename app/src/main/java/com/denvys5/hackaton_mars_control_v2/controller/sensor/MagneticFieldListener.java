package com.denvys5.hackaton_mars_control_v2.controller.sensor;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;

import com.denvys5.hackaton_mars_control_v2.controller.SensorsDataSink;

public class MagneticFieldListener implements SensorEventListener {
    private SensorsDataSink sensorsDataSink;

    public MagneticFieldListener() {
        sensorsDataSink = SensorsDataSink.getInstance();
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        sensorsDataSink.setMagnetometerData(event.values);
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }
}
