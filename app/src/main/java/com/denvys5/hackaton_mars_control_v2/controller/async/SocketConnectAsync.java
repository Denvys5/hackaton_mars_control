package com.denvys5.hackaton_mars_control_v2.controller.async;

import android.os.AsyncTask;

import com.denvys5.hackaton_mars_control_v2.Config;

import java.io.IOException;
import java.net.Socket;

public class SocketConnectAsync extends AsyncTask<Void, Void, Socket>{
    private Socket socket;
    private boolean connected;

    public boolean isConnected() {
        return connected;
    }

    @Override
    protected Socket doInBackground(Void... voids) {
        while(!isCancelled() && !isConnected()) {
            try {
                connectToServer();
                connected = true;
            } catch (IOException e) {
                e.printStackTrace();
                try {
                    Thread.sleep(100);
                } catch (InterruptedException ex) {
                    ex.printStackTrace();
                }
            }
        }
        return socket;
    }

    @Override
    protected void onCancelled() {
        super.onCancelled();
        try {
            socket.close();
            connected = false;
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void connectToServer() throws IOException {
        socket = new Socket(Config.serverAddress, Config.port);

    }
}
