package com.denvys5.hackaton_mars_control_v2.controller.async;

import android.os.AsyncTask;

import java.io.BufferedReader;
import java.io.IOException;

public class SocketInputAsync extends AsyncTask<BufferedReader, Integer, Long> {
    @Override
    protected Long doInBackground(BufferedReader... inputs) {
        BufferedReader in = inputs[0];
        while (!isCancelled()) {
            String response;
            try {
                while (in.ready()) {
                    try {
                        response = in.readLine();
                        if (response == null || response.equals("")) {
                            continue;
                            //System.exit(0);
                        }
                    } catch (IOException ex) {
                        response = "Error: " + ex;
                    }
//                    System.out.println(response);
                }
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        }
        try {
            Thread.sleep(150);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return null;
    }
}
