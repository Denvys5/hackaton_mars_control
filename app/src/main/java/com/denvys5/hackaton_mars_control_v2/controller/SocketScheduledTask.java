package com.denvys5.hackaton_mars_control_v2.controller;

import com.denvys5.hackaton_mars_control_v2.Config;
import com.denvys5.hackaton_mars_control_v2.model.JsonMessageDTO;
import com.google.gson.Gson;

public class SocketScheduledTask extends Thread{
    private SocketController socketController;

    public SocketScheduledTask() {
        socketController = SocketController.getInstance();
    }

    public void run(){
        Gson gson = new Gson();
        while(!isInterrupted()){
            JsonMessageDTO jsonMessageDTO = MessageDataHolder.getInstance().getMessage();
            socketController.println(gson.toJson(jsonMessageDTO));
            try {
                Thread.sleep(Config.scheduleTime);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
