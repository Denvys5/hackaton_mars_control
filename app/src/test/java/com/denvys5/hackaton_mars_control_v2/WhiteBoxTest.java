package com.denvys5.hackaton_mars_control_v2;

import android.hardware.SensorManager;


import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.BDDMockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import static junit.framework.TestCase.assertEquals;

@RunWith(PowerMockRunner.class)
@PrepareForTest(SensorManager.class)
public class WhiteBoxTest {
    private float accX;
    private float accY;
    private float accZ;
    private float axisX;
    private float axisY;
    private float axisZ;
    private float magX;
    private float magY;
    private float magZ;


    @Test
    public void generatePosition(){
        // Rotation matrix based on current readings from accelerometer and magnetometer.
        final float[] rotationMatrix = new float[9];

        PowerMockito.mockStatic(SensorManager.class);
        BDDMockito.given(SensorManager.getRotationMatrix(rotationMatrix, null,
                new float[]{accX, accY, accZ}, new float[]{magX, magY, magZ})).willAnswer(invocation -> {
            rotationMatrix[0] = accX;
            rotationMatrix[1] = accY;
            rotationMatrix[2] = accZ;
            rotationMatrix[3] = magX;
            rotationMatrix[4] = magY;
            rotationMatrix[5] = magZ;
            rotationMatrix[6] = 1;
            rotationMatrix[7] = 1;
            rotationMatrix[8] = 1;
            return null;
        });

        SensorManager.getRotationMatrix(rotationMatrix, null,
                new float[]{accX, accY, accZ}, new float[]{magX, magY, magZ});

        // Express the updated rotation matrix as three orientation angles.
        final float[] orientationAngles = new float[3];

        BDDMockito.given(SensorManager.getOrientation(rotationMatrix, orientationAngles)).willAnswer(invocation -> {
            orientationAngles[0] = 1;
            orientationAngles[1] = 1;
            orientationAngles[2] = 1;
            return null;
        });

        SensorManager.getOrientation(rotationMatrix, orientationAngles);

        axisX = (float) (orientationAngles[0]*Math.PI/180D);
        axisY = (float) (orientationAngles[1]*Math.PI/180D);
        axisZ = (float) (orientationAngles[2]*Math.PI/180D);


        assertEquals(axisX, axisY);
    }
}
